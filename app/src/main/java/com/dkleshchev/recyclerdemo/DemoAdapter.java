package com.dkleshchev.recyclerdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dkleshchev.recyclerdemo.helper.ItemTouchHelperAdapter;
import com.dkleshchev.recyclerdemo.models.Card;
import com.dkleshchev.recyclerdemo.models.Type;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import rx.Observable;
import rx.subjects.PublishSubject;

@AllArgsConstructor
public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.ViewHolder>
        implements ItemTouchHelperAdapter {
    private List<Card> items;
    private final PublishSubject<Integer> onClickSubject = PublishSubject.create();

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().ordinal();
    }

    public Type getCardType(final int viewType) {
        return Type.values()[viewType];
    }

    public Card getItem(final int position) {
        if (items.size() > position) {
            return items.get(position);
        }
        return null;
    }

    public boolean isFirstGroupIndex(final int position) {
        for (int i = 0; i < items.size(); i++) {
            final Card current = items.get(i);

            switch (current.getType()) {

                case PICTURE:
                    break;
                case TEXT:
                    if (i == 0) {
                        return true;
                    } else if (i == position) {
                        final Card previous = items.get(i - 1);
                        return previous.getType() != Type.TEXT;
                    }
            }
        }

        return false;
    }

    @Override
    public void onItemDismiss(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(items, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Type cardType = getCardType(viewType);
        switch (cardType) {

            case PICTURE:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
                return new ViewHolder(view);
            case TEXT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text, parent, false);
                return new ViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(view -> onClickSubject.onNext(holder.getAdapterPosition()));
    }

    public Observable<Integer> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
