package com.dkleshchev.recyclerdemo.models;

import lombok.Getter;

public abstract class Card {
    @Getter
    protected Type type;
}
