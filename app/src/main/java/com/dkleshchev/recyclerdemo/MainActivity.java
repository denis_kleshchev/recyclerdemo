package com.dkleshchev.recyclerdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.dkleshchev.recyclerdemo.decorator.ImageDecorator;
import com.dkleshchev.recyclerdemo.decorator.TextDecorator;
import com.dkleshchev.recyclerdemo.decorator.VerticalSpaceDecorator;
import com.dkleshchev.recyclerdemo.helper.SimpleItemTouchHelperCallback;
import com.dkleshchev.recyclerdemo.models.Card;
import com.dkleshchev.recyclerdemo.models.PictureCard;
import com.dkleshchev.recyclerdemo.models.TextCard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

@SuppressWarnings("CreateIntent")
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recycler)
    RecyclerView recycler;

    private DemoAdapter adapter;
    private Subscription subscription;

    private final List<Card> cards = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initItems();
        adapter = new DemoAdapter(cards);
        recycler.setChildDrawingOrderCallback((childCount, i) -> childCount - i - 1);
        recycler.setLayoutManager(new CustomLayoutManager());
        ItemTouchHelper touchHelper = new ItemTouchHelper(new SimpleItemTouchHelperCallback(adapter));
        touchHelper.attachToRecyclerView(recycler);
//        recycler.setLayoutManager(new LinearLayoutManager(this));
        setDecorators();
//        addSnap();
        recycler.setAdapter(adapter);
        ItemClickSupport.addTo(recycler).setOnItemClickListener((recyclerView, position, v) -> Log.d("OnItemClick", "Item click: " + position));
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscription = adapter.getPositionClicks().subscribe(position -> {
            Log.d("OnItemClick", "Item click: " + position);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    private void setDecorators() {
        recycler.addItemDecoration(new ImageDecorator());
        recycler.addItemDecoration(new TextDecorator());
        recycler.addItemDecoration(new VerticalSpaceDecorator(20));
    }

    private void initItems() {
        cards.add(new PictureCard());
        cards.add(new TextCard());
        cards.add(new TextCard());
        cards.add(new TextCard());
        cards.add(new PictureCard());
        cards.add(new TextCard());
        cards.add(new TextCard());
        cards.add(new TextCard());
        cards.add(new PictureCard());
    }

    private void addSnap() {
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recycler);
    }
}
