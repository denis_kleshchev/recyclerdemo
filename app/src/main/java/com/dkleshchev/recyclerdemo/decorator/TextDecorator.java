package com.dkleshchev.recyclerdemo.decorator;

import android.support.v7.widget.RecyclerView;

import com.dkleshchev.recyclerdemo.DemoAdapter;
import com.dkleshchev.recyclerdemo.models.Type;

public class TextDecorator extends RootTopDecorator {
    @Override
    protected Type getType() {
        return Type.TEXT;
    }

    @Override
    protected boolean isDecorated(RecyclerView.Adapter adapter, int position, int itemViewType) {
        if (itemViewType != getType().ordinal() || !(adapter instanceof DemoAdapter)) {
            return false;
        }

        final DemoAdapter mAdapter = (DemoAdapter) adapter;

        return mAdapter.isFirstGroupIndex(position);
    }
}
