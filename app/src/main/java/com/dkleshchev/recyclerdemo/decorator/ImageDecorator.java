package com.dkleshchev.recyclerdemo.decorator;

import android.support.v7.widget.RecyclerView;

import com.dkleshchev.recyclerdemo.models.Type;

public class ImageDecorator extends RootTopDecorator {
    @Override
    protected Type getType() {
        return Type.PICTURE;
    }

    @Override
    protected boolean isDecorated(RecyclerView.Adapter adapter, int position, int itemViewType) {
        return itemViewType == getType().ordinal();
    }
}
