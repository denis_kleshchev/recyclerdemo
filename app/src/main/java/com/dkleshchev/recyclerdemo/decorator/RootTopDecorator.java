package com.dkleshchev.recyclerdemo.decorator;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dkleshchev.recyclerdemo.DemoAdapter;
import com.dkleshchev.recyclerdemo.R;
import com.dkleshchev.recyclerdemo.models.Type;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class RootTopDecorator extends RecyclerView.ItemDecoration {
    private final int verticalOffset = 20;
    protected View spacer = null;

    protected abstract Type getType();

    protected abstract boolean isDecorated(final RecyclerView.Adapter adapter, final int position, final int itemViewType);

    protected void fixLayoutSize(View view, ViewGroup parent) {
        // Check if the view has a layout parameter and if it does not createWithBody one for it
        if (view.getLayoutParams() == null) {
            view.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        // Create a width and height spec using the parent as an example:
        // For width we make sure that the item matches exactly what it measures from the parent.
        //  IE if layout says to match_parent it will be exactly parent.getWidth()
        int widthSpec = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        // For the height we are going to createWithBody a spec that says it doesn't really care what is calculated,
        //  even if its larger than the screen
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        // Get the child specs using the parent spec and the padding the parent has
        int childWidth = ViewGroup.getChildMeasureSpec(widthSpec,
                parent.getPaddingLeft() + parent.getPaddingRight(), view.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec,
                parent.getPaddingTop() + parent.getPaddingBottom(), view.getLayoutParams().height);

        // Finally we measure the sizes with the actual view which does margin and padding changes to the sizes calculated
        view.measure(childWidth, childHeight);

        // And now we setup the layout for the view to ensure it has the correct sizes.
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);

        if (position == RecyclerView.NO_POSITION) {
            return;
        }

        final DemoAdapter adapter = (DemoAdapter) parent.getAdapter();
        final int itemViewType = adapter.getItemViewType(position);

        if (spacer == null) {
            spacer = getSpacer(parent, getType());
        }

        if (isDecorated(adapter, position, itemViewType)) {
            outRect.top = spacer.getHeight() + verticalOffset;
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int childCount = parent.getChildCount();
        for (int i=0; i<childCount; i++) {
            View child = parent.getChildAt(i);

            final int childAdapterPosition = parent.getChildAdapterPosition(child);
            final DemoAdapter adapter = (DemoAdapter) parent.getAdapter();

            if (childAdapterPosition < 0 || childAdapterPosition >= adapter.getItemCount()) {
                return;
            }

            final Type cardType = adapter.getCardType(adapter.getItemViewType(childAdapterPosition));

            if (!isDecorated(adapter, childAdapterPosition, cardType.ordinal())) {
                continue;
            }

            c.save();
            c.translate(0, child.getTop() - spacer.getHeight() - verticalOffset);
            spacer.draw(c);
            c.restore();
        }
    }

    private View getSpacer(final RecyclerView parent, final Type cardType) {
        View spacer = LayoutInflater.from(parent.getContext()).inflate(getViewLayoutId(cardType), parent, false);
        fixLayoutSize(spacer, parent);

        return spacer;
    }

    protected int getViewLayoutId(final Type type) {
        switch (type){

            case PICTURE:
                return R.layout.header_image;
            case TEXT:
                return R.layout.header_text;
        }

        return 0;
    }
}
